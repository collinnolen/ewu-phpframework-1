<?php

// app.php

// Create the application
$app = new Silex\Application();

// Load Environment
$config_dir = __DIR__.'/../config';
$config_file = '.env';
$dotenv = new Dotenv\Dotenv($config_dir);
if (file_exists($config_dir.'/'.$config_file)) {
    $dotenv->load();
}
//$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

// Set debug
if (strcasecmp(getenv('DEBUG'), 'true') == 0) {
    $app['debug'] = true;
}

// Register DBAL Service Provider
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
            'driver'    => getenv('FORMS_DB_DRIVER'),
            'host'      => getenv('FORMS_DB_HOST'),
            'dbname'    => getenv('FORMS_DB_DBNAME'),
            'user'      => getenv('FORMS_DB_USER'),
            'password'  => getenv('FORMS_DB_PASSWORD'),
    ),
));

// Register ORM Service Provider
$app->register(new Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), array(
    'orm.proxies_dir' => __DIR__.'/../db/proxies',
    'orm.em.options' => array(
        'mappings' => array(
            array(
                'type' => 'annotation',
                'namespace' => 'Forms\Test',
                'path' => __DIR__.'/../src/models',
//                'use_simple_annotation_reader' => false,
            ),
        ),
    ),
));

// Register a Twig Service Provider
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register a Translation Provider (needed for default form view)
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

// Register a Validation Service Provider
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Register a Form Service Provider
$app->register(new Silex\Provider\FormServiceProvider());

// Load Middleware for Routes
require_once __DIR__.'/middleware/sso.php';

// Add routes
$app->match('/', function () use ($app) {

    return 'Hello World';
});

// Add SSO route with middleware
$app->match('/sso', function () {
    return 'You are authenticated';
})
->before($ssoProtect)
;

// Add Employee Only route with middleware
$app->match('/employee', function () {
    return 'You are an authenticated employee';
})
->before($employees_only)
;

// Add an specific Controller Set
$app->mount('/test', include 'controllers/test.php');

return $app;
