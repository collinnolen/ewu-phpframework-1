<?php

// bootstrap.php
require_once __DIR__.'/../vendor/autoload.php';

$app = include __DIR__.'/app.php';

return $app;
